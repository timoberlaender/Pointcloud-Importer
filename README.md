# Pointcloud Importer 

This plugin provides an improved design for managing pointclouds in GroIMP. It includes a base design as well as a simple implementation. It also enables importing PLY files as pointcloud.

It includes support of the file type :
- ".ply" 

