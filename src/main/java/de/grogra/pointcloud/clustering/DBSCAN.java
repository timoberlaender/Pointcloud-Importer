package de.grogra.pointcloud.clustering;

import java.util.ArrayList;
import java.util.List;

import de.grogra.graph.impl.Node;
import de.grogra.pointcloud.objects.PointCloudBase;
import de.grogra.pointcloud.objects.impl.LeafPointImpl;
import de.grogra.pointcloud.objects.impl.PointCloudBaseImpl;
import de.grogra.pointcloud.utils.Utils;

public abstract class DBSCAN {

	/***
	 * Clusters the given point cloud with the DBSCAN algorithm by the given parameters
	 * @param pc point cloud to be clustered
	 * @param epsilon The maximum distance for two points that are in the same
	 * cluster
	 * @param minimumNeighbours The minimum number of neighbors that must exist
	 * for a point to be no noise-cluster point.
	 * @param octreeDepth The number of layers in the octree that is used to
	 * cluster the point cloud. This must be > 1 to decrease the runtime of the
	 * clustering algorithm.
	 * @param removeNoiseCluster The indicator that can be set to true to remove
	 * all points that are not explicitly in an other point cloud.
	 * @return A list of point clouds representing the cluster
	 */
	public static List<PointCloudBaseImpl> cluster(PointCloudBaseImpl pc, double epsilon, int minimumNeighbours, int octreeDepth, boolean removeNoiseCluster){
		List<ClusterPoint> points = DBSCAN.getLeavesAsPointArray(pc);
		List<PointCloudBaseImpl> clusters = new ArrayList<>();
		PointCloudOctree octree = new PointCloudOctree(pc, points, octreeDepth);
		PointCloudBaseImpl noise = new PointCloudBaseImpl("", pc.getBalancindFactor(), pc.getDepth(), LeafPointImpl.class);
		
		for(int i = 0; i < points.size(); i++) {
			ClusterPoint curr = points.get(i);
			if(curr.getPointCloud() == null) {
				List<ClusterPoint> neighbourList = octree.getNeighbors(curr, epsilon);
				if(neighbourList.size() < minimumNeighbours) {
					curr.setPointCloud(noise);
					//curr.getLeaf().removeEdgeBits(Graph.BRANCH_EDGE, null);
					curr.getLeaf().remove(null);
					noise.addPoint(curr.getLeaf());
					continue;
				}
				PointCloudBaseImpl cluster = new PointCloudBaseImpl("", pc.getBalancindFactor(), pc.getDepth(), LeafPointImpl.class);
				List<ClusterPoint> neighbors = new ArrayList<ClusterPoint>();
				neighbors.add(curr);
				addNeighborsToCluster(octree, cluster, neighbors, epsilon);
				clusters.add(cluster);
			}
		}
		if(!removeNoiseCluster) {
			clusters.add(noise);
		}
		
		return clusters;
	}
	
	/**
	 * Searches for all points in the octree that are neighbored to points of
	 * the given neighbors list and adds all neighbored points to that list and
	 * the current point cloud. Points are neighbored if the distance (epsilon)
	 * between both points is lower than the given epsilon parameters.
	 *
	 * @param octree The octree to get the points from
	 * @param pointCloud The point cloud to put the points to
	 * @param neighbors The list of currently checked points
	 * @param epsilon The maximum distance that must be between two points to be
	 * neighbored
	 */
	private static void addNeighborsToCluster(PointCloudOctree octree, PointCloudBaseImpl pointCloud, List<ClusterPoint> neighbors, double epsilon) {
		if (neighbors.size() == 0) {
			return;
		}
		ClusterPoint point = neighbors.get(0);
		List<ClusterPoint> currentNeighbors = octree.getNeighbors(point, epsilon);
		neighbors.remove(0);
		//point.getLeaf().removeEdgeBits(Graph.BRANCH_EDGE, null);
		point.getLeaf().remove(null);
		pointCloud.addPoint(point.getLeaf());
		point.setPointCloud(pointCloud);
		int index = 0;
		while (index < currentNeighbors.size()) {
			ClusterPoint neighbor = currentNeighbors.get(index);
			if (neighbor.getPointCloud() == null) {
				neighbor.setPointCloud(pointCloud);
				neighbors.add(neighbor);
				addNeighborsToCluster(octree, pointCloud, neighbors, epsilon);
			}
			index++;
		}
	}
	
	/***
	 * Converts the leaf nodes of the point cloud to ClusterPoints 
	 * 
	 * @return a list containing the leaves as ClusterPoints
	 */
	public static List<ClusterPoint> getLeavesAsPointArray(PointCloudBase pc){
		List<Node> leafs = Utils.getLeafNodes(pc);
		List<ClusterPoint> points = new ArrayList<>();
		for(int i = 0; i < leafs.size(); i++){
			Node n = leafs.get(i);
			if(n instanceof LeafPointImpl){
				LeafPointImpl point = (LeafPointImpl)n;
				ClusterPoint p = new ClusterPoint(point);
				points.add(p);
			}
		}
		return points;
	}
	
}
