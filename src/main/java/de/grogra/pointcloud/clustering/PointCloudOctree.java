package de.grogra.pointcloud.clustering;

import java.util.ArrayList;
import java.util.List;

import de.grogra.pointcloud.objects.impl.PointCloudBaseImpl;

public class PointCloudOctree {

	public static final int OCTREE_CHILDREN = 8;
	
	private PointCloudOctree[] children;
	
	private List<ClusterPoint> points;
	/**
	 * The minimum x position of this octree
	 */
	private double minimumX;
	/**
	 * The minimum y position of this octree
	 */
	private double minimumY;
	/**
	 * The minimum z position of this octree
	 */
	private double minimumZ;
	/**
	 * The maximum x position of this octree
	 */
	private double maximumX;
	/**
	 * The maximum y position of this octree
	 */
	private double maximumY;
	/**
	 * The maximum z position of this octree
	 */
	private double maximumZ;
	/**
	 * The layer of this octree
	 */
	private int layer;
	/**
	 * A constructor to get an octree object. The layer is defined as specified
	 * in the parameter. The points are declared as an empty list. The children
	 * array is initialized.
	 *
	 * @param layer The layer of this octree
	 */
	public PointCloudOctree(int layer) {
		this.children = new PointCloudOctree[PointCloudOctree.OCTREE_CHILDREN];
		this.points = new ArrayList<ClusterPoint>();
		this.layer = layer;
	}
	
	public PointCloudOctree(PointCloudBaseImpl pc, List<ClusterPoint> points, int depth) {
		this(0);
		List<ClusterPoint> tmp = new ArrayList<ClusterPoint>();
		for(int i = 0; i<points.size(); i++) {
			tmp.add(points.get(i));
		}
		
		setPoints(tmp);
		setMinimumX(pc.getMinimumX());
		setMinimumY(pc.getMinimumY());
		setMinimumZ(pc.getMinimumZ());
		setMaximumX(pc.getMaximumX());
		setMaximumY(pc.getMaximumY());
		setMaximumZ(pc.getMaximumZ());
		movePointsToLayer(depth);
	}
	
	/**
	 * Sets one of the eight children (one of the eight octants). The octant
	 * parameter must be in the range of 0 to 7.
	 *
	 * @param octant The octant number (0 - 7)
	 * @param child The child octree that should be added
	 */
	public void setChild(int octant, PointCloudOctree child) {
		this.children[octant] = child;
	}
	/**
	 * Returns the octree child on the given octant (0 - 7). If the octant was
	 * never declared or set to null, the returned object will also be null.
	 *
	 * @param octant The number of the wanted octant (0 - 7)
	 * @return The octant child element or null
	 */
	public PointCloudOctree getChild(int octant) {
		return this.children[octant];
	}
	/**
	 * Returns the number of non-null child elements. Because the octree has
	 * always eight places to store child elements, only the used children are
	 * counted.
	 *
	 * @return The number of used child elements
	 */
	public int getNumberOfChildren() {
		int index = 0;
		int number = 0;
		while (index < PointCloudOctree.OCTREE_CHILDREN) {
			if (this.children[index] != null) {
				number++;
			}
			index++;
		}
		return number;
	}
	/**
	 * Moves the points of this octree to the respective octree or octrees on
	 * the given layer. If the layer is lower than the layer of this octree, the
	 * points are collected and moved to the given parent node. If the layer is
	 * higher than the layer of this octree node, the points are distributed to
	 * the fitting child elements.
	 *
	 * @param layer The target layer (where to move the points to)
	 */
	public void movePointsToLayer(int layer) {
		this.distributePointsToChildren(layer);
		this.collectPointsFromChildren(layer);
	}
	/**
	 * Distributes the points of this octree to the child elements until they
	 * have reached the given target layer.
	 *
	 * @param layer The target layer (where to move the points to)
	 */
	private void distributePointsToChildren(int layer) {
		if (this.layer >= layer) {
			return;
		}
		while (this.points.size() > 0) {
			int octant = this.determineOctantForPoint(this.points.get(0));
			if (this.children[octant] == null) {
				this.createChild(octant);
			}
			this.children[octant].addPoint(this.points.get(0));
			this.points.remove(0);
		}
		int index = 0;
		while (index < PointCloudOctree.OCTREE_CHILDREN) {
			if (this.children[index] != null) {
				this.children[index].distributePointsToChildren(layer);
			}
			index++;
		}
	}
	/**
	 * Collects the points of all child elements of this octree and moves them
	 * to this octree.
	 *
	 * @param layer The target layer (where to move the points to)
	 */
	private void collectPointsFromChildren(int layer) {
		int index = 0;
		while (index < PointCloudOctree.OCTREE_CHILDREN) {
			PointCloudOctree child = this.children[index];
			if (child != null) {
				child.collectPointsFromChildren(layer);
				if (this.layer >= layer) {
					this.points.addAll(child.getPoints());
					this.children[index] = null;
				}
			}
			index++;
		}
	}
	/**
	 * Determines and returns the correct octree child element (octant) for the
	 * given point. The point is categorized by its position and the positions
	 * of the child octrees. If the given point is not contained in this octree
	 * and not in the child octrees, 0 is returned.
	 *
	 * @param point The point to get the octant for
	 * @return The octant number for the point or 0 in case of a point outside
	 * this octree node
	 */
	private int determineOctantForPoint(ClusterPoint point) {
		int octant = 0;
		if (point.getX() >= this.getCenterX()) {
			octant += 4;
		}
		if (point.getY() >= this.getCenterY()) {
			octant += 2;
		}
		if (point.getZ() >= this.getCenterZ()) {
			octant += 1;
		}
		return octant;
	}
	/**
	 * Returns the neighbor points for the given point. An other point is a
	 * neighbor point if the distance between both points is less than the given
	 * distance. If there are no neighbor points, an empty list is returned.
	 *
	 * @param point The point to get the neighbor points for
	 * @param distance The maximum distance to identify other points as neighbor
	 * points
	 * @return The list of neighbor points or an empty list in case of no
	 * neighbor points
	 */
	public List<ClusterPoint> getNeighbors(ClusterPoint point, double distance) {
		List<PointCloudOctree> leaves = new ArrayList<PointCloudOctree>();
		PointCloudOctree.findNeighborLeaves(point, distance, this, leaves);
		List<ClusterPoint> neighbors = new ArrayList<ClusterPoint>();
		int index = 0;
		while (index < leaves.size()) {
			PointCloudOctree tree = leaves.get(index);
			List<ClusterPoint> points = tree.getPoints();
			int pointIndex = 0;
			while (pointIndex < points.size()) {
				ClusterPoint otherPoint = points.get(pointIndex);
				if (point != otherPoint && point.getDistanceToPoint(otherPoint) < distance) {
					neighbors.add(otherPoint);
				}
				pointIndex++;
			}
			index++;
		}
		return neighbors;
	}
	/**
	 * Fills the given list of leaves (octree nodes) with all neighbored octree
	 * nodes that have at least the given maximum distance to the given point.
	 * This method works recursively, so all neighbored octree nodes are
	 * contained in the given list in the end. This list is used to get all
	 * potential neighbor points of the given point.
	 *
	 * @param point The point to get the neighbored octree leaf nodes for
	 * @param distance The maximum distance between the point and an octree node
	 * to be detected as neighbors
	 * @param tree The octree to search in
	 * @param leaves The list of leaf octree elements. The found elements are
	 * added here.
	 */
	private static void findNeighborLeaves(ClusterPoint point, double distance, PointCloudOctree tree, List<PointCloudOctree> leaves) {
		if (!PointCloudOctree.intersects(point, distance, tree)) {
			return;
		}
		if (tree.getNumberOfChildren() > 0) {
			int index = 0;
			while (index < PointCloudOctree.OCTREE_CHILDREN) {
				if (tree.getChild(index) != null) {
					PointCloudOctree.findNeighborLeaves(point, distance, tree.getChild(index), leaves);
				}
				index++;
			}
		} else {
			leaves.add(tree);
		}
	}
	/**
	 * Returns true if the given point located is in the given octree child. If
	 * the distance between the point and the child octree is lower than the
	 * given distance parameter, the point is also detected as intersecting
	 * point. The point must have the correct coordinates in x, y and z
	 * direction. Otherwise, false is returned.
	 *
	 * @param point The point to check
	 * @param distance The maximum distance between the octree child and the
	 * point to let the point be an intersecting point
	 * @param child The child octree that must match to the point
	 * @return true If the point is located inside the given octree child or has
	 * at least the given maximum distance to it. Otherwise, false is returned.
	 */
	private static boolean intersects(ClusterPoint point, double distance, PointCloudOctree child) {
		double minimumX = child.getMinimumX() - distance;
		double maximumX = child.getMaximumX() + distance;
		double minimumY = child.getMinimumY() - distance;
		double maximumY = child.getMaximumY() + distance;
		double minimumZ = child.getMinimumZ() - distance;
		double maximumZ = child.getMaximumZ() + distance;
		boolean xIntersects = point.getX() >= minimumX && point.getX() <= maximumX;
		boolean yIntersects = point.getY() >= minimumY && point.getY() <= maximumY;
		boolean zIntersects = point.getZ() >= minimumZ && point.getZ() <= maximumZ;
		return xIntersects && yIntersects && zIntersects;
	}
	/**
	 * Creates a child octree in the given octant.
	 *
	 * @param octant The octant number
	 */
	private void createChild(int octant) {
		PointCloudOctree octree = new PointCloudOctree(this.layer + 1);
		if (octant < 4) {
			// x = lower half (0, 1, 2, 3)
			octree.setMinimumX(this.minimumX);
			octree.setMaximumX(this.getCenterX());
		} else {
			// x = upper half (4, 5, 6, 7)
			octree.setMinimumX(this.getCenterX());
			octree.setMaximumX(this.maximumX);
		}
		if ((octant / 2) % 2 == 0) {
			// y = lower half (0, 1, 4, 5)
			octree.setMinimumY(this.minimumY);
			octree.setMaximumY(this.getCenterY());
		} else {
			// y = upper half (2, 3, 6, 7)
			octree.setMinimumY(this.getCenterY());
			octree.setMaximumY(this.maximumY);
		}
		if (octant % 2 == 0) {
			// z = lower half (0, 2, 4, 6)
			octree.setMinimumZ(this.minimumZ);
			octree.setMaximumZ(this.getCenterZ());
		} else {
			// z = upper half (1, 3, 5, 7)
			octree.setMinimumZ(this.getCenterZ());
			octree.setMaximumZ(this.maximumZ);
		}
		this.children[octant] = octree;
	}
	/**
	 * Sets the layer of this octree (in relation to the root octree).
	 *
	 * @param layer The new layer for this octree
	 */
	public void setLayer(int layer) {
		this.layer = layer;
	}
	/**
	 * Returns the layer of this octree (in relation to the root octree).
	 *
	 * @return The layer of this octree
	 */
	public int getLayer() {
		return this.layer;
	}
	/**
	 * Sets the points of this octree.
	 *
	 * @param points The new poionts for this octree
	 */
	public void setPoints(List<ClusterPoint> points) {
		this.points = points;
	}
	/**
	 * Adds a point to this octree.
	 *
	 * @param point The point to add
	 */
	public void addPoint(ClusterPoint point) {
		this.points.add(point);
	}
	/**
	 * Returns the list of points of this octree
	 *
	 * @return The list of points of this octree
	 */
	public List<ClusterPoint> getPoints() {
		return this.points;
	}
	/**
	 * Returns the center x position of this octree. The center x position is
	 * the middle between the minimum x position and the maximum x position.
	 *
	 * @return The center x position
	 */
	public double getCenterX() {
		return this.minimumX + (this.maximumX - this.minimumX) / 2;
	}
	/**
	 * Returns the center y position of this octree. The center y position is
	 * the middle between the minimum y position and the maximum y position.
	 *
	 * @return The center y position
	 */
	public double getCenterY() {
		return this.minimumY + (this.maximumY - this.minimumY) / 2;
	}
	/**
	 * Returns the center z position of this octree. The center z position is
	 * the middle between the minimum z position and the maximum z position.
	 *
	 * @return The center z position
	 */
	public double getCenterZ() {
		return this.minimumZ + (this.maximumZ - this.minimumZ) / 2;
	}
	/**
	 * Sets the minimum x value of this octree.
	 *
	 * @param minimum The new minimum x value for this octree
	 */
	public void setMinimumX(double minimum) {
		this.minimumX = minimum;
	}
	/**
	 * Returns the minimum x value of this octree.
	 *
	 * @return The minimum x value of this octree
	 */
	public double getMinimumX() {
		return this.minimumX;
	}
	/**
	 * Sets the minimum y value of this octree.
	 *
	 * @param minimum The new minimum y value for this octree
	 */
	public void setMinimumY(double minimum) {
		this.minimumY = minimum;
	}
	/**
	 * Returns the minimum y value of this octree.
	 *
	 * @return The minimum y value of this octree
	 */
	public double getMinimumY() {
		return this.minimumY;
	}
	/**
	 * Sets the minimum z value of this octree.
	 *
	 * @param minimum The new minimum z value for this octree
	 */
	public void setMinimumZ(double minimum) {
		this.minimumZ = minimum;
	}
	/**
	 * Returns the minimum z value of this octree.
	 *
	 * @return The minimum z value of this octree
	 */
	public double getMinimumZ() {
		return this.minimumZ;
	}
	/**
	 * Sets the maximum x value of this octree.
	 *
	 * @param maximum The new maximum x value for this octree
	 */
	public void setMaximumX(double maximum) {
		this.maximumX = maximum;
	}
	/**
	 * Returns the maximum x value of this octree.
	 *
	 * @return The maximum x value of this octree
	 */
	public double getMaximumX() {
		return this.maximumX;
	}
	/**
	 * Sets the maximum y value of this octree.
	 *
	 * @param maximum The new maximum y value for this octree
	 */
	public void setMaximumY(double maximum) {
		this.maximumY = maximum;
	}
	/**
	 * Returns the maximum y value of this octree.
	 *
	 * @return The maximum y value of this octree
	 */
	public double getMaximumY() {
		return this.maximumY;
	}
	/**
	 * Sets the maximum z value of this octree.
	 *
	 * @param maximum The new maximum z value for this octree
	 */
	public void setMaximumZ(double maximum) {
		this.maximumZ = maximum;
	}
	/**
	 * Returns the maximum z value of this octree.
	 *
	 * @return The maximum z value of this octree
	 */
	public double getMaximumZ() {
		return this.maximumZ;
	}
}
