package de.grogra.pointcloud.importer;

import de.grogra.graph.impl.Node;
import de.grogra.pf.io.FilterBase;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pointcloud.objects.PointCloudLeaf;
import de.grogra.xl.util.XHashMap;

public abstract class PointCloudFilterBase extends FilterBase {

	protected Node pcRoot;
	protected XHashMap<Long, Node> allPoints=new XHashMap<Long, Node>();
	protected boolean addPoints=false;
	protected LeafMap leaves;
	
	public PointCloudFilterBase(FilterItem item, FilterSource source) {
		super(item, source);
		leaves = new LeafMap (item);
	}
	
	/**
	 * @param object type of leaf considered - point, line, mesh
	 * @param asNode is object a node or an edge?
	 * @return suitable export for the object or <code>null</code> 
	 */
	public PointCloudLeaf getLeafFor (Object object, boolean asNode)
	{
		return (PointCloudLeaf) leaves.eval (object.toString());
	}
	
	public abstract void endExport();

	public abstract Node getPoint(long id);

	public abstract Node[] getPoints(Object list);
	
	/**
	 * Provide a mapping between the required keys from the leaf elements and the filter.
	 * @param o the required key from a leaf
	 * @return
	 */
	public abstract String getKey(Object o);
}
