package de.grogra.pointcloud.ui;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import javax.swing.JOptionPane;
import de.grogra.graph.Graph;
import de.grogra.graph.impl.Edge;
import de.grogra.graph.impl.GraphManager;
import de.grogra.graph.impl.Node;
import de.grogra.imp.IMP;
import de.grogra.imp3d.View3D;
import de.grogra.imp3d.objects.Plane;
import de.grogra.mesh.renderer.CollectionMesh;
import de.grogra.pf.registry.Item;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.UIProperty;
import de.grogra.pf.ui.Workbench;
import de.grogra.pf.ui.edit.GraphSelection;
import de.grogra.pointcloud.clustering.DBSCAN;
import de.grogra.pointcloud.clustering.KMeans;
import de.grogra.pointcloud.objects.PointCloudBase;
import de.grogra.pointcloud.objects.PointCloudLeaf;
import de.grogra.pointcloud.objects.impl.CollectionCloudImpl;
import de.grogra.pointcloud.objects.impl.PointCloudBaseImpl;
import de.grogra.pointcloud.utils.Utils;
import de.grogra.rgg.RGGRoot;

import javax.swing.JPanel;

/**
 * This class contains methods useful for ui interactions.
 * All Methods are static.
 * 
 * @author Nico Hundertmark
 */
public class UI {
	
	/**
	 * This method is called from the user by the user interface. It splits the selected point cloud by a selected plane.
	 * 
	 * @param item The item clicked
	 * @param information Information about the clicked menu entry
	 * @param context The context where the item was clicked
	 */
	public static void splitByPlane(Item item, Object information, Context context) {
		List<Node> objects = getSelectedNodes(context, false);
		
		if(objects.size() != 2) 
			return;
		
		PointCloudBase pointCloud = null;
		Plane plane = null;
		if (objects.get(0) instanceof PointCloudBase && objects.get(1) instanceof Plane) {
			pointCloud = (PointCloudBase)(objects.get(0));
			plane = (Plane)(objects.get(1));
		} else if (objects.get(0) instanceof Plane && objects.get(1) instanceof PointCloudBase) {
			pointCloud = (PointCloudBase)(objects.get(1));
			plane = (Plane)(objects.get(0));
		} else if (objects.get(0) instanceof CollectionMesh && objects.get(1) instanceof Plane) {
			pointCloud = getPointCloudFromCollectionMesh(objects.get(0));
			plane = (Plane)(objects.get(1));
		} else if (objects.get(0) instanceof Plane && objects.get(1) instanceof CollectionMesh) {
			pointCloud = getPointCloudFromCollectionMesh(objects.get(1));
			plane = (Plane)(objects.get(0));
		}
		else {
			return;
		}
		
		PointCloudBase pc = pointCloud.split(plane);
		addPointCloudToGraph(pc, context);
		
	}

	/**
	 * This method is called from the user by the user interface. It splits a selected point cloud by all selected
	 * point cloud leaves.
	 * 
	 * @param item The item clicked
	 * @param information Information about the clicked menu entry
	 * @param context The context where the item was clicked
	 */
	public static void splitByLeaves(Item item, Object information, Context context) {
		List<Node> objects = getSelectedNodes(context, true);
		
		if(objects.size() < 2)
			return;
		
		PointCloudBase pointcloud = null;
		List<PointCloudLeaf> leaves = new ArrayList<PointCloudLeaf>();
		
		for(int i = 0; i < objects.size(); i++) {
			if(objects.get(i) instanceof PointCloudBase)
				pointcloud = (PointCloudBase) objects.get(i);
			else if(objects.get(i) instanceof PointCloudLeaf)
				leaves.add((PointCloudLeaf) objects.get(i));
		}
		
		if(pointcloud != null && leaves.size() > 0) {
			PointCloudBase pc = pointcloud.split(leaves.toArray(new PointCloudLeaf[0]));
			addPointCloudToGraph(pc, context);
		}
	}
	
	/**
	 * This method is called from the user by the user interface. It will merge all selected point clouds
	 * into the first selected point cloud.
	 * 
	 * @param item The item clicked
	 * @param information Information about the clicked menu entry
	 * @param context The context where the item was clicked
	 */
	public static void merge(Item item, Object information, Context context) {
		List<Node> objects = getSelectedNodes(context, false);
		
		if(objects.size() < 2)
			return;
		
		List<PointCloudBase> pointclouds = new ArrayList<PointCloudBase>();
		for(int i = 0; i < objects.size(); i++) {
			if(objects.get(i) instanceof CollectionMesh)
				pointclouds.add(getPointCloudFromCollectionMesh(objects.get(i)));
			else if(objects.get(i) instanceof PointCloudBase)
				pointclouds.add((PointCloudBase) objects.get(i));
		}
		
		if(pointclouds.size() >= 2) {
			PointCloudBase pc = pointclouds.get(0);
			for(int i = 1; i < pointclouds.size(); i++) {
				pc.merge(pointclouds.get(i));
			}
			((CollectionMesh)pc.getFirstEdge().getSource()).setUpdate(true);
		}
		
	}
	
	/**
	 * This method is called from the user by the user interface. It will determine the selected point cloud
	 * and calls the K-Means clustering algorithm with user input data as parameters.
	 * 
	 * @param item The item clicked
	 * @param information Information about the clicked menu entry
	 * @param context The context where the item was clicked
	 */
	public static void kMeansCluster(Item item, Object information, Context context) {
		List<Node> objects = getSelectedNodes(context, false);
		
		PointCloudBaseImpl pointcloud;
		if(objects.size() != 1) {
			return;
		}
		
		if(objects.get(0) instanceof PointCloudBaseImpl)
			pointcloud = (PointCloudBaseImpl) objects.get(0);
		else if(objects.get(0) instanceof CollectionMesh)
			pointcloud = (PointCloudBaseImpl) getPointCloudFromCollectionMesh(objects.get(0));
		else
			return;
		
		List<String> labels = List.of("Cluster Count", "Maximum Iterations");
        List<Class<?>> types = List.of(Integer.class, Integer.class);
        
        JPanel panel = UIDialog.createDynamicInputPanel(labels, types);
        String title = "KMeans clustering";
		int buttons = JOptionPane.OK_CANCEL_OPTION;
		int type = JOptionPane.QUESTION_MESSAGE;
		int result = UIDialog.getInstance().displayParameterInput(panel, title, buttons, type);
		if(result == JOptionPane.OK_OPTION) {
			List<Object> inputs = UIDialog.getInputValues(panel, types);
			int clusterCount = (int) inputs.get(0);
			int maxIterationsCount = (int) inputs.get(1);
			
			PointCloudBaseImpl[] cluster = KMeans.cluster(pointcloud, clusterCount, maxIterationsCount);
		
			//remove the current point cloud
			pointcloud.getAxisParent().getAxisParent().removeAll(null);
			
			//cluster zum Graph hinzufügen
			for(int i = 0; i < cluster.length; i++) {
				addPointCloudToGraph(cluster[i], context);
			}
		}
	}
	
	/**
	 * This method is called from the user by the user interface. It will determine the selected point cloud
	 * and calls the DBSCAN clustering algorithm with user input data as parameters.
	 * 
	 * @param item The item clicked
	 * @param information Information about the clicked menu entry
	 * @param context The context where the item was clicked
	 */
	public static void dbscanCluster(Item item, Object information, Context context) {
		List<Node> objects = getSelectedNodes(context, false);
		
		PointCloudBaseImpl pointcloud;
		if(objects.size() != 1) {
			return;
		}
		
		if(objects.get(0) instanceof PointCloudBaseImpl)
			pointcloud = (PointCloudBaseImpl) objects.get(0);
		else if(objects.get(0) instanceof CollectionMesh)
			pointcloud = (PointCloudBaseImpl) getPointCloudFromCollectionMesh(objects.get(0));
		else
			return;
		
		List<String> labels = List.of("Point distance", "Minimum Neighbours", "Octree Depth", "Remove Noise");
        List<Class<?>> types = List.of(Double.class, Integer.class, Integer.class, Boolean.class);
        
        JPanel panel = UIDialog.createDynamicInputPanel(labels, types);
        String title = "DBSACN clustering";
		int buttons = JOptionPane.OK_CANCEL_OPTION;
		int type = JOptionPane.QUESTION_MESSAGE;
		int result = UIDialog.getInstance().displayParameterInput(panel, title, buttons, type);
		if(result == JOptionPane.OK_OPTION) {
			List<Object> inputs = UIDialog.getInputValues(panel, types);
			double eps = (double) inputs.get(0);
			int minimumNeighbours = (int) inputs.get(1);
			int octreeDepth = (int) inputs.get(2);
			boolean removeNoise = (boolean) inputs.get(3);
			
			List<PointCloudBaseImpl> clusters = DBSCAN.cluster(pointcloud, eps, minimumNeighbours, octreeDepth, removeNoise);
		
			//remove the current point cloud
			pointcloud.getAxisParent().getAxisParent().removeAll(null);
			
			//adding the clusters to the scene
			for(int i = 0; i < clusters.size(); i++) {
				addPointCloudToGraph(clusters.get(i), context);
			}
			
		}
	}
	
	
	/**
	 * Adds a point cloud to the current context's graph
	 * 
	 * @param pc. Pointcloud which will be added
	 * @param context. The current context
	 */
	private static void addPointCloudToGraph(PointCloudBase pc, Context context) {
		Node pcRoot = new CollectionCloudImpl();
		CollectionMesh cm = new CollectionMesh();
		cm.addEdgeBitsTo(pc, Graph.BRANCH_EDGE, null);
		pcRoot.addEdgeBitsTo(cm, Graph.BRANCH_EDGE, null);
		GraphManager g = context.getWorkbench().getRegistry().getProjectGraph();
		RGGRoot.getRoot(g).addEdgeBitsTo(pcRoot, Graph.BRANCH_EDGE, null);
		//IMP.addNode(null, pcRoot, context);
	}
	
	/***
	 * Returns the first point cloud from a CollectionMesh node 
	 * @param object The CollectionMesh node
	 * @return The first point cloud contained in the CollectionMesh
	 */
	private static PointCloudBase getPointCloudFromCollectionMesh(Object object) {
		if(!(object instanceof CollectionMesh))
			return null;
		CollectionMesh collectionMesh = (CollectionMesh) object;
		Node[] nodes = Utils.fromRoot(collectionMesh);
		return (PointCloudBase) nodes[0];
	}
	
	/**
	 * Returns the root nodes of the displayed object graph of the current 3D
	 * view in the given context. There are multiple root nodes in the graph,
	 * but normally only the first one (with index 0) is relevant. It contains
	 * an RGG root node as direct child element and contains the displayed
	 * objects generated by XL scripts.
	 *
	 * @param context The current context where the method is executed
	 * @return The array of root nodes of the displayed graph
	 */
	public static Node[] getGraphRootNodes(Context context) {
		View3D view3d = View3D.getDefaultView(context);
		Graph graph = view3d.getGraph();
		String[] keys = graph.getRootKeys();
		if (keys == null || keys.length == 0) {
			return new Node[0];
		}
		Node[] nodes = new Node[keys.length];
		int index = 0;
		while (index < keys.length) {
			Object object = graph.getRoot(keys[index]);
			Node node = (Node)(object);
			nodes[index] = node;
			index++;
		}
		return nodes;
	}
	
	/**
	 * Returns a list that contains all objects of the RGG tree that are
	 * currently selected in the workbench. All root nodes are considered. If
	 * no objects are selected, an empty list is returned. The objects are not
	 * sorted by their tree structure.
	 *
	 * @param context The current context of the workbench
	 * @param searchLeafs Flag if the all leafs gets searched
	 * @return The list of selected objects
	 */
	public static List<Node> getSelectedNodes(Context context, boolean searchLeafs) {
		Node startNode = getGraphRootNodes(context)[0];
		List<Node> nodes = new ArrayList<Node>();
		Queue<Node> queue = new LinkedList<Node>();
		HashSet<Node> visited = new HashSet<Node>();
		queue.add(startNode);
		visited.add(startNode);
		
		while(queue.size() > 0) {
			startNode = queue.poll();
			//Node source = edge.getSource();
			
			//get all adejacentNodes
			Edge edge = startNode.getFirstEdge();
			while(edge != null) {
				if(edge.isSource(startNode) && (edge instanceof Node)) {
					
					if(!visited.contains((Node)edge)) {
						if (isNodeSelected((Node)edge))
							nodes.add((Node)edge);
						visited.add((Node)edge);
						
						if(edge instanceof PointCloudBase) {
							if(searchLeafs) {
								queue.add((Node)edge);
							}
						}else {
							queue.add((Node)edge);							
						}
					}
				}
				edge = edge.getNext(startNode);
			}
		}
		return nodes;
	}
	
	/**
	 * Returns true if the given node is selected in the selection of the
	 * current workbench. Otherwise, false is returned.
	 *
	 * @param node The node that should be checked
	 * @return true If the node is selected and false otherwise
	 */
	public static boolean isNodeSelected(Node node) {
		Workbench workbench = Workbench.current();
		if (workbench == null) {
			return false;
		}
		Object object = UIProperty.WORKBENCH_SELECTION.getValue(workbench);
		if (!(object instanceof GraphSelection)) {
			return false;
		}
		GraphSelection selection = (GraphSelection)(object);
		return selection.contains(node.getGraph(), node, true);
	}
}
