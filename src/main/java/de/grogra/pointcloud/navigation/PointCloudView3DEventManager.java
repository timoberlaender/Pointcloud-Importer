package de.grogra.pointcloud.navigation;

import de.grogra.imp.viewhandler.EmptyHighlighter;
import de.grogra.imp.viewhandler.SelectionEventFactory;
import de.grogra.imp.viewhandler.ToolEventFactory;
import de.grogra.imp.viewhandler.ViewEventFactory;
import de.grogra.imp3d.Navigator3DFactory;
import de.grogra.imp3d.PickToolVisitor;
import de.grogra.persistence.SCOType;

public class PointCloudView3DEventManager extends ViewEventFactory {
	//enh:sco SCOType
	
		public PointCloudView3DEventManager() {
			this.navigatorFactory = new Navigator3DFactory();
			this.toolFactory = new ToolEventFactory();
			this.selectionFactory = new SelectionEventFactory();
			this.highlighter = new EmptyHighlighter();
			
			setPickVisitor(new PickRayVisitor ().setResolution(2));
			setPickToolVisitor(new PickToolVisitor ());
		}
			
		//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;


	public static class Type extends SCOType
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (PointCloudView3DEventManager representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, SCOType.$TYPE);
		}


		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		public Object newInstance ()
		{
			return new PointCloudView3DEventManager ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (PointCloudView3DEventManager.class);
		$TYPE.validate ();
	}

//enh:end
}
