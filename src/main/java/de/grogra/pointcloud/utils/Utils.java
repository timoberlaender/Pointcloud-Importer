package de.grogra.pointcloud.utils;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import javax.vecmath.Matrix4d;
import javax.vecmath.Tuple3d;
import javax.vecmath.Vector3d;

import de.grogra.graph.EdgePatternImpl;
import de.grogra.graph.Graph;
import de.grogra.graph.impl.Edge;
import de.grogra.graph.impl.Extent;
import de.grogra.graph.impl.GraphManager;
import de.grogra.graph.Path;
import de.grogra.graph.VisitorImpl;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.Null;
import de.grogra.imp3d.objects.Plane;
import de.grogra.pointcloud.objects.PointCloudBase;
import de.grogra.pointcloud.objects.PointCloudLeaf;
import de.grogra.pointcloud.objects.impl.PointCloudBaseImpl;

public class Utils {
	
	
	static class TranslateVisitor extends VisitorImpl {
		
		Tuple3d translate;
		
		TranslateVisitor (Tuple3d tup){
			super();
			this.translate = tup;
		}
		@Override
		public Object visitEnter (Path path, boolean node)
		{
			if (super.visitEnter(path, node)!=null) {
				return STOP;
			}
			if (node)
			{
				Node n = (Node) path.getObject (-1);
				if (n instanceof PointCloudLeaf && n instanceof Null) {
					Vector3d v = ((Null)n).getTranslation();
					((Null)n).setTranslation(v.x+translate.x, v.y+translate.y, v.z+translate.z);
				}
			}
			return null;
		}
		
		@Override
		public Object visitInstanceEnter ()
		{
			return STOP;
		}
	}

	/**
	 * translate the pointcloud with the translation vector.
	 * 
	 * @param pc
	 * @param coordinate the new origin (a node that is going to be pushed to the (0,0,0)
	 */
	public static void translate(PointCloudBase pc, Tuple3d translation) {
		TranslateVisitor v = new TranslateVisitor(translation);
		v.init(pc.getCurrentGraphState(),  new EdgePatternImpl
				(Graph.BRANCH_EDGE | Graph.SUCCESSOR_EDGE, Graph.REFINEMENT_EDGE, true, true));
		pc.getGraph().accept(pc, v, null);
	}
	
	public static void translate(Node[] pcs, Tuple3d translation) {
		for (Node pc : pcs) {
			if (pc instanceof PointCloudBase) {
				translate((PointCloudBase)pc, translation);
			}
		}
	}
	
	/**
	 * Translate the pointcloud base to use the coordinate as new origin based on the newOrigin leaf provided.
	 * @param pc
	 * @param newOrigin
	 * @param coordinate
	 */
	public static void setAsOrigin(PointCloudBase pc, PointCloudLeaf newOrigin, Tuple3d coordinate ) {
		if (!(newOrigin instanceof Null)) {
			return;
		}
		Vector3d t = ((Null)newOrigin).getTranslation();
		Vector3d translation = new Vector3d();
		translation.x = coordinate.x - t.x;
		translation.y = coordinate.y - t.y;
		translation.z = coordinate.z - t.z;
		
		TranslateVisitor v = new TranslateVisitor(translation);
		v.init(pc.getCurrentGraphState(),  new EdgePatternImpl
				(Graph.BRANCH_EDGE | Graph.SUCCESSOR_EDGE, Graph.REFINEMENT_EDGE, true, true));
		pc.getGraph().accept(pc, v, null);
	}
	
	public static void setAsOrigin(Node[] pcs, PointCloudLeaf newOrigin, Tuple3d coordinate ) {
		for (Node pc : pcs) {
			if (pc instanceof PointCloudBase) {
				setAsOrigin((PointCloudBase)pc, newOrigin, coordinate);
			}
		}
	}
	
	
	/**
	 * Return the PointCloudBase that have the given source name (i.e. the pointcloud created from a file).
	 * One file usually create several pointcloudbase objects (one for the points, one for the faces, one for the edges).
	 * @param name
	 * @return
	 */
	public static Node[] fromSourceName(Graph g, String name) {
		name = Paths.get(name).getFileName().toString();
		
		ArrayList<Node> result = new ArrayList<Node>();
		Extent e = ((GraphManager)g).getExtent(PointCloudBaseImpl.class);
		for (Node n = e.getFirstNode (0); n != null; n = e.getNextNode (n))
		{
			if (n instanceof PointCloudBase && 
					 Paths.get( ((PointCloudBase)n).getSourceName()).getFileName().toString().equals(name) ) {
				result.add(n);
			}
		}
		return result.toArray(new Node[0]);
	}
	
	/**
	 * Return the PointCloudBase that are one depth bellow the given root.
	 * When imported pointclouds file create several PointCloudBase that are linked to a node
	 * before being added to the main graph. That node is the one expected here.
	 * @param root
	 */
	public static Node[] fromRoot(Node root) {
		Set<Node> pcs = new HashSet<Node>();
		
		for (Edge e = root.getFirstEdge (); e != null; e = e.getNext (root))
		{
			Node t = e.getSource();
			
			
			int b = e.getEdgeBits ();
			if ( (((b & Graph.SUCCESSOR_EDGE) != 0) || ((b & Graph.BRANCH_EDGE) != 0))
					&& t == root 
					&& e.getTarget() instanceof PointCloudBase)
			{
				pcs.add(e.getTarget());
			}
			
		}
		
		return pcs.toArray(new Node[0]);
	}

	/***
	 * Determine whether a leaf node is above or under a given plane
	 * @param leaf The leaf to be checked
	 * @param plane The plane to check against
	 * @return true if the leaf is over the given plane else false
	 */
	public static boolean isLeafOverPlane(Null leaf, Plane plane) {
		Matrix4d matrix = plane.getLocalTransformation();
		Vector3d myPoint = new Vector3d(leaf.getTranslation());
		Vector3d position = plane.getTranslation();
		Vector3d normal = new Vector3d(matrix.m02, matrix.m12, matrix.m22);
		position.sub(myPoint);
		return position.dot(normal) > 0;
	}
	
	/***
	 * Returns all leaf nodes as list of nodes
	 * @param pc the point cloud containing the leaf nodes
	 * @return list of nodes
	 */
	public static List<Node> getLeafNodes(PointCloudBase pc) {
		Node startNode = pc;
		List<Node> nodes = new ArrayList<Node>();
		Queue<Node> queue = new LinkedList<Node>();
		HashSet<Node> visited = new HashSet<Node>();
		queue.add(startNode);
		visited.add(startNode);
		
		while(queue.size() > 0) {
			startNode = queue.poll();
			//Node source = edge.getSource();
			
			//get all adejacentNodes
			Edge edge = startNode.getFirstEdge();
			while(edge != null) {
				if(edge.isSource(startNode) && (edge instanceof Node)) {
					if(!visited.contains((Node)edge)) {
						if (edge instanceof PointCloudLeaf)
							nodes.add((Node)edge);
						visited.add((Node)edge);
						queue.add((Node)edge);
					}
				}
				edge = edge.getNext(startNode);
			}
		}
		return nodes;
	}
}
