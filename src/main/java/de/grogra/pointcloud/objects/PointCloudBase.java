package de.grogra.pointcloud.objects;

import de.grogra.graph.Attribute;
import de.grogra.imp3d.objects.Plane;
import de.grogra.imp3d.objects.Null;

/**
 * pointcloudBase represent a point cloud object. Which should be the root of 
 * a graph where leaves are the "points".
 * "Points" can be any object that implements pointcloudpoint
 */
public abstract class PointCloudBase extends Null {
	
	/*
	 * Add a point to the pointcloud
	 */
	public abstract void addPoint(PointCloudLeaf[] nodes);
	public abstract void addPoint(PointCloudLeaf node);

	/*
	 * Remove a point from the pointcloud
	 */
	public abstract void removePoint(Object id);
	
	public abstract void removePoint(PointCloudLeaf[] nodes);
	
	/*
	 * Rebalance the graph based on the numbers of node
	 */
	public abstract void rebalance();
	
	
	/*
	 * merge pointcloud pc into this
	 */
	public abstract void merge(PointCloudBase pc);
	
	/*
	 * remove the nodes from the pointclouds and add them to a new one 
	 */
	public abstract PointCloudBase split(PointCloudLeaf[] nodes);
	
	/*
	 * splits the point cloud with a given plane
	 */ 
	public abstract PointCloudBase split(Plane plane);
	
	/*
	 * Split by attribute A - return a set of pointcloudroot (same childrenType) where each 
	 * PC only have point with same attribute A value
	 */
	public abstract PointCloudBase[] split(Attribute a);
	
	/*
	 * Name of the source file / attribute from source? 
	 */
	public abstract String getSourceName();
	
	/*
	 * Implementation type of the nodes (points). By default use {PointCloudNode} - to be defined
	 */
	protected Class<? extends PointCloudLeaf> childrenType;
	protected abstract Class getChildrenType() throws ClassNotFoundException;
	
	/*
	 * The number of leaf in this graph = number of point in the pointcloud
	 */
	protected long numberOfChild;
	
	/*
	 * Balancing is done by spreading points to "abstract" intermediate nodes. This 
	 * factor represent the maximum children an intermediate node can have.
	 */
	protected long balancingFactor;
	
	
	public abstract PointCloudLeaf getFirstLeaf();
}
