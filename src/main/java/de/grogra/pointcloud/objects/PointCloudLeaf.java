package de.grogra.pointcloud.objects;

import javax.vecmath.Vector3d;

import de.grogra.graph.impl.Node;

/*
 * All points in pointclouds should implements this AND extends Node
 * Probably should be a class. The shape should be a attribute (e.g. representativeType = Sphere).
 */
public interface PointCloudLeaf{
	
	public abstract Node getNode();
	
	/*
	 * The id of the object from the import file - The same one is used in the point cloud
	 */
	public long getIdImport();
	public void setIdImport (long value);
	
	/*
	 *  Import properties other that (x,y,z,id,color) require custom class that extends this
	 *  All additional attributes should be called pc_NAME_OF_THE_ATTRIBUTE
	 */
	public Vector3d getLocation();

}
