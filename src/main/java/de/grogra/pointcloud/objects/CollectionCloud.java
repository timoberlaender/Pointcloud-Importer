package de.grogra.pointcloud.objects;

import de.grogra.imp.View;
import de.grogra.imp3d.RenderState;
import de.grogra.xl.util.FloatList;

public interface CollectionCloud {
	
	/** 
	 * give the vertices of the pointcloud
	 */
	public abstract FloatList getVertices(RenderState rs, View v);
}
