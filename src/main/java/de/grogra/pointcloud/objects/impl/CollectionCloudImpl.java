package de.grogra.pointcloud.objects.impl;

import javax.vecmath.Vector3d;

import de.grogra.graph.impl.Edge;
import de.grogra.imp.View;
import de.grogra.imp3d.RenderState;
import de.grogra.imp3d.objects.Attributes;
import de.grogra.imp3d.objects.Null;
import de.grogra.pointcloud.objects.CollectionCloud;
import de.grogra.pointcloud.objects.IntermediateCloudNode;
import de.grogra.pointcloud.objects.PointCloudBase;
import de.grogra.pointcloud.objects.PointCloudLeaf;
import de.grogra.xl.util.FloatList;

/**
 */
public class CollectionCloudImpl extends Null implements CollectionCloud {

	@Override
	public FloatList getVertices(RenderState rs, View v) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void translateToOrigin() {
		Vector3d v= getFirstBase().getFirstLeaf().getLocation();
		setTranslation(-v.x, -v.y, -v.z);
	}

	public PointCloudBase getFirstBase() {
		Edge e = getFirstEdge();
		while( !(e instanceof PointCloudBase) && e!=null) {
			e = e.getNext(e.getTarget());
			if ( !(e instanceof PointCloudBase) && 
					!(e instanceof IntermediateCloudNode) && 
					!(e instanceof PointCloudLeaf)) {
				return null;
			}
		}
		return (PointCloudBase) e;
	}
	
	private static void initType ()
	{
		$TYPE.addIdentityAccessor (Attributes.TRANSFORMATION);
	}
	
	//enh:insert initType ();
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;


	static
	{
		$TYPE = new NType (new CollectionCloudImpl ());
		initType ();
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new CollectionCloudImpl ();
	}

//enh:end
}
