package de.grogra.pointcloud.objects.impl;

import java.util.Arrays;

import javax.vecmath.Vector3d;

import de.grogra.graph.Graph;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.Attributes;
import de.grogra.imp3d.objects.MeshNode;
import de.grogra.imp3d.objects.PolygonMesh;
import de.grogra.imp3d.objects.Polygons;
import de.grogra.persistence.ManageableType.Field;
import de.grogra.pointcloud.importer.PointCloudFilterBase;
import de.grogra.pointcloud.objects.PointCloudLeaf;
import de.grogra.util.StringMap;
import de.grogra.xl.util.FloatList;
import de.grogra.xl.util.IntList;

public class LeafMeshImpl extends MeshNode implements PointCloudLeaf{

	protected long idImport;
	//enh:field setter getter
	
	public enum Keys{
		/**
		 * Required list of ids
		 */
		VERTEX_IDS,
		/**
		 * Optional colored arguments
		 */
		RED,
		GREEN,
		BLUE,
		ALPHA
	}
	
	public LeafMeshImpl ()
	{
		super ();
		setLayer (4);
	}
	
	public LeafMeshImpl (StringMap args)
	{
		this (getNodesFromArgs(args));
		
		Float[] colors = getColorFromArgs(args);
		if (colors.length==3 && !(Arrays.asList(colors).contains(-1f))) {
			setColor(colors[0], colors[1], colors[2]);
		}
		removeRequiredArgs(args);
		for (String k : args.getKeys()) {
			Field f = this.getNType().getManagedField(k);
			if ( f!=null) {
				f.set (this, new int[] {f.getFieldId()}, args.get(k), null);
			}
		}
	}

	public LeafMeshImpl (Node[] nodes)
	{
		this ( Arrays.stream(nodes).toArray(LeafPointImpl[]::new)  );
	}

	public LeafMeshImpl (Polygons polygons)
	{
		super (polygons);
	}

	public LeafMeshImpl (LeafPointImpl[] points)
	{
		super (getMesh(points));
		//TODO: this would work if the collectorMesh includes transformation
		// setTranslation(points[0].getTranslation().x, points[0].getTranslation().y, points[0].getTranslation().z);
		for (LeafPointImpl p : points) {
			this.addEdgeBitsTo(p, Graph.REFINEMENT_EDGE, null);
		}
	}
	
	private static Node[] getNodesFromArgs(StringMap args) {
		PointCloudFilterBase filter = (PointCloudFilterBase) args.get("ctx");
		return filter.getPoints(args.get(filter.getKey(Keys.VERTEX_IDS)));
	}
	
	private static Float[] getColorFromArgs(StringMap args) {
		PointCloudFilterBase filter = (PointCloudFilterBase) args.get("ctx");
		return new Float[] {args.getFloat(filter.getKey(Keys.RED), -1f), 
			args.getFloat( filter.getKey(Keys.GREEN), -1f), 
			args.getFloat( filter.getKey(Keys.BLUE), -1f) };
	}
	
	/**
	 * Delete args values that are linked with "keys" key
	 */
	private void removeRequiredArgs(StringMap args) {
		PointCloudFilterBase filter = (PointCloudFilterBase) args.get("ctx");
		for (Keys k : Keys.values()) {
			args.remove( filter.getKey(k));
		}
	}
	
	@Override
	public Node getNode() {
		return this;
	}
	
	public static PolygonMesh getMesh (LeafPointImpl[] points) {
		FloatList vertexData = new FloatList();
		//translate to the frist point, then remove this translation to each
		// other node
		LeafPointImpl firstNode = points[0];
		for (LeafPointImpl p : points) {
			// TODO: thsi would work if the CollectorMesh uses local transfo
//			vertexData.add((float)p.getTranslation().x-(float)firstNode.getTranslation().x);
//			vertexData.add((float)p.getTranslation().y-(float)firstNode.getTranslation().y);
//			vertexData.add((float)p.getTranslation().z-(float)firstNode.getTranslation().z);
			vertexData.add((float)p.getTranslation().x);
			vertexData.add((float)p.getTranslation().y);
			vertexData.add((float)p.getTranslation().z);

		}
		//construct a data type, which can handle a list of triangulated mash
		PolygonMesh polygonMesh = new PolygonMesh();
		polygonMesh.setIndexData(new IntList(new int[] {0,1,2}));
		// set the list of vertices
		polygonMesh.setVertexData(vertexData);
		return polygonMesh;
	}
	
	public Vector3d getLocation() {
		return getTranslation() ;
	}
	
	private static void initType ()
	{
		$TYPE.addIdentityAccessor (Attributes.SHAPE);
	}

	//enh:insert initType ();
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;

	public static final NType.Field idImport$FIELD;

	private static final class _Field extends NType.Field
	{
		private final int id;

		_Field (String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			super (LeafMeshImpl.$TYPE, name, modifiers, type, componentType);
			this.id = id;
		}

		@Override
		public void setLong (Object o, long value)
		{
			switch (id)
			{
				case 0:
					((LeafMeshImpl) o).idImport = (long) value;
					return;
			}
			super.setLong (o, value);
		}

		@Override
		public long getLong (Object o)
		{
			switch (id)
			{
				case 0:
					return ((LeafMeshImpl) o).getIdImport ();
			}
			return super.getLong (o);
		}
	}

	static
	{
		$TYPE = new NType (new LeafMeshImpl ());
		$TYPE.addManagedField (idImport$FIELD = new _Field ("idImport", _Field.PROTECTED  | _Field.SCO, de.grogra.reflect.Type.LONG, null, 0));
		initType ();
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new LeafMeshImpl ();
	}

	public long getIdImport ()
	{
		return idImport;
	}

	public void setIdImport (long value)
	{
		this.idImport = (long) value;
	}

//enh:end
}
