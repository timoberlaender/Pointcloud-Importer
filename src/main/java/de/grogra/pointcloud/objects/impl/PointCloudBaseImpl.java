package de.grogra.pointcloud.objects.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import de.grogra.graph.Attribute;
import de.grogra.graph.Graph;
import de.grogra.pointcloud.objects.IntermediateCloudNode;
import de.grogra.pointcloud.objects.PointCloudBase;
import de.grogra.pointcloud.objects.PointCloudLeaf;
import de.grogra.util.Utils;
import de.grogra.graph.impl.Edge;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.objects.Attributes;
import de.grogra.imp3d.objects.Null;
import de.grogra.imp3d.objects.Plane;


/**
 * Handling adding nodes and their persistence should be managed by a graph manager and a persistence 
 * manager.
 */
public class PointCloudBaseImpl extends PointCloudBase {

	protected String sourceName;
	//enh:field getter
	
	protected String childrenClass;
	//enh:field getter
	
	IntermediateCloudNode lastIPCN;
	long depth;
	long minId;
	long maxId;
	
	public PointCloudBaseImpl() {
		this.numberOfChild=0;
		this.sourceName="";
		this.balancingFactor=-1;
		this.childrenType=null;
		this.depth=0;
		this.childrenClass="";
	}
	
	public PointCloudBaseImpl(String name, long balancingFactor, long depth, Class type) {
		this.numberOfChild=0;
		this.sourceName=name;
		this.balancingFactor=balancingFactor;
		this.childrenType=type;
		this.depth=depth;
		this.childrenClass=childrenType.getName();
		
		// add the first path to the Leaves
		createPathToLeaves(this, 0);
	}
	
	/**
	 * create a list of intermediate nodes of the depth required starting from depth d. and add the 
	 * path to "node"
	 * 
	 */
	private void createPathToLeaves(Node node, long d) {
		Node s = node;
		IntermediateCloudNodeImpl t;
		while ((d=d+1)<=depth) {
			t = new IntermediateCloudNodeImpl(d);
			s.addEdgeBitsTo(t, Graph.BRANCH_EDGE, null);
			s=t;
			this.lastIPCN =t;
		}
	}
	
	/**
	 * get the depth of the tree based on the number of leaves n, and the degree of the tree
	 * @param n
	 * @param d
	 * @return
	 */
	public static long computeDepthFromNandD(long n, long d) {
		long depth=1;
		while ( (n = n/d+(n%d==0?0:1)) > d) {
			depth++;
		}
		return depth;
	}
	
	
	@Override
	public void addPoint(PointCloudLeaf[] nodes) {
		for (PointCloudLeaf p : nodes) {
			addPoint(p);
		}
	}
	
	@Override
	public void addPoint(PointCloudLeaf node) {
		boolean addedOrDiscared=false;
		while(!addedOrDiscared) {
			//while the lastICN is not full > add to him. 
			if (lastIPCN.getNumberChildren()<balancingFactor) {
				lastIPCN.addChild(node.getIdImport());
				lastIPCN.addEdgeBitsTo(node.getNode(), Graph.BRANCH_EDGE, null);
				addedOrDiscared=true;
				numberOfChild++;
			} else {
				Node parent = lastIPCN.getAxisParent();
				if (parent==this && getDirectChildCount()>balancingFactor) {
					//Something when wrong - the graph is full but some not are not added
					// for now discard them
					System.out.println("SHOULDN't BE there. Some nodes have been discarded");
					addedOrDiscared=true;
				} else {
					createPathToLeaves(parent, lastIPCN.getLevel()-1);
				}
			}
		}
	}

	/**
	 * args is the set of arguments that the leaf constructor takes in.
	 * e.g. for pointcloudpointimpl it is float x, float y, float z
	 * @param args
	 * @return
	 */
	public PointCloudLeaf createLeaf(Object[] args) {
		Object o=null;
		try {
			o = Utils.invoke (getChildrenType().getName(), args, getChildrenType().getClassLoader());
		} catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException
				| InvocationTargetException e) {
			e.printStackTrace();
		}
		if (o instanceof PointCloudLeaf) {
			return (PointCloudLeaf)o;
		}
		return null;
	}

	
	protected Class<? extends PointCloudLeaf> getChildrenType() throws ClassNotFoundException{
		return (childrenType != null) ? childrenType : (Class<? extends PointCloudLeaf>) Class.forName( this. childrenClass);
	}
	
	//TODO:
	@Override
	public void removePoint(Object id) {
		numberOfChild--;

	}
	//TODO:
	@Override
	public void removePoint(PointCloudLeaf[] nodes) {
		numberOfChild--;

	}

	@Override
	public void rebalance() {
		// TODO Auto-generated method stub
	}

	
	/***
	 * Merges the leaf nodes of the specified point cloud into the current point cloud.
	 * @param pc The point cloud from which leaf nodes will be merged.
	 */
	@Override
	public void merge(PointCloudBase pc) {
		List<Node> leafNodes = de.grogra.pointcloud.utils.Utils.getLeafNodes(pc);
		for(int i = 0; i<leafNodes.size(); i++)
			leafNodes.get(i).remove(null);
			//leafNodes.get(i).removeEdgeBits(Graph.BRANCH_EDGE, null);
		this.addPoint(leafNodes.toArray(new PointCloudLeaf[0]));
	}

	/***
	 * Splits the point cloud by a set of leaves. Removes the leaves 
	 * from the point cloud and creates a new point cloud.
	 * @param nodes An Array containing leaf nodes 
	 * @return PointCloudBase A new point cloud containing the leaves
	 */
	@Override
	public PointCloudBase split(PointCloudLeaf[] nodes) {
		PointCloudBase pc = new PointCloudBaseImpl("", balancingFactor, depth, childrenType);
		List<Node> childNodes = de.grogra.pointcloud.utils.Utils.getLeafNodes(this);
		
		for(int j = 0; j<nodes.length; j++) {
			PointCloudLeaf leaf = nodes[j];
			for(int i = 0; i<childNodes.size(); i++) {
				PointCloudLeaf childLeaf = (PointCloudLeaf)childNodes.get(i);
				if(leaf.equals(childLeaf)) {
					this.removePoint(leaf);
					//childNodes.get(i).removeEdgeBits(Graph.BRANCH_EDGE, null);
					childNodes.get(i).remove(null);
					pc.addPoint(leaf);
				}
			}
		}
		return pc;
	}
	
	/***
	 * Splits the point cloud by a given plane. Removes the leaves
	 * from the point cloud and creates a new point cloud.
	 * @param plane to be used as cutting object
	 * @return PointCloudBase A new point cloud containing the leaves above the plane
	 */
	@Override
	public PointCloudBase split(Plane plane) {
		PointCloudBaseImpl pc = new PointCloudBaseImpl("", balancingFactor, depth, childrenType);
		List<Node> childNodes = de.grogra.pointcloud.utils.Utils.getLeafNodes(this);
		
		for(int i = 0; i < childNodes.size(); i++) {
			if(!(childNodes.get(i) instanceof PointCloudLeaf))
				continue;
			
			PointCloudLeaf leaf = (PointCloudLeaf) childNodes.get(i);
			if(de.grogra.pointcloud.utils.Utils.isLeafOverPlane((Null)leaf, plane)) {
				this.removePoint(leaf);
				//childNodes.get(i).removeEdgeBits(Graph.BRANCH_EDGE, null);
				childNodes.get(i).remove(null);
				pc.addPoint(leaf);
			}
		}
		
		return pc;
	}
	
	/***
	 * Calculates and returns the minimum X-coordinate value among all leaf nodes.
	 * @return The minimum X-coordinate value among all leaf nodes, or 0 if no leaf nodes are present.
	 */
	public double getMinimumX() {
		List<Node> leafs = de.grogra.pointcloud.utils.Utils.getLeafNodes(this);
		if(leafs.size() == 0)
			return 0;
		double minX = ((Null)leafs.get(0)).getTranslation().x;
		for(int i = 0; i < leafs.size(); i++) {
			if(minX > ((Null)leafs.get(i)).getTranslation().x) {
				minX = ((Null)leafs.get(i)).getTranslation().x;
			}
		}
		return minX;
	}
	
	/***
	 * Calculates and returns the minimum Y-coordinate value among all leaf nodes.
	 * @return The minimum Y-coordinate value among all leaf nodes, or 0 if no leaf nodes are present.
	 */
	public double getMinimumY() {
		List<Node> leafs = de.grogra.pointcloud.utils.Utils.getLeafNodes(this);
		if(leafs.size() == 0)
			return 0;
		double minY = ((Null)leafs.get(0)).getTranslation().y;
		for(int i = 0; i < leafs.size(); i++) {
			if(minY > ((Null)leafs.get(i)).getTranslation().y) {
				minY = ((Null)leafs.get(i)).getTranslation().y;
			}
		}
		return minY;
	}
	
	/***
	 * Calculates and returns the minimum Z-coordinate value among all leaf nodes.
	 * @return The minimum Z-coordinate value among all leaf nodes, or 0 if no leaf nodes are present.
	 */
	public double getMinimumZ() {
		List<Node> leafs = de.grogra.pointcloud.utils.Utils.getLeafNodes(this);
		if(leafs.size() == 0)
			return 0;
		double minZ = ((Null)leafs.get(0)).getTranslation().z;
		for(int i = 0; i < leafs.size(); i++) {
			if(minZ > ((Null)leafs.get(i)).getTranslation().z) {
				minZ = ((Null)leafs.get(i)).getTranslation().z;
			}
		}
		return minZ;
	}
	
	/***
	 * Calculates and returns the maximum X-coordinate value among all leaf nodes.
	 * @return The maximum X-coordinate value among all leaf nodes, or 0 if no leaf nodes are present.
	 */
	public double getMaximumX() {
		List<Node> leafs = de.grogra.pointcloud.utils.Utils.getLeafNodes(this);
		if(leafs.size() == 0)
			return 0;
		double maxX = ((Null)leafs.get(0)).getTranslation().x;
		for(int i = 0; i < leafs.size(); i++) {
			if(maxX < ((Null)leafs.get(i)).getTranslation().x) {
				maxX = ((Null)leafs.get(i)).getTranslation().x;
			}
		}
		return maxX;
	}
	
	/***
	 * Calculates and returns the maximum Y-coordinate value among all leaf nodes.
	 * @return The maximum Y-coordinate value among all leaf nodes, or 0 if no leaf nodes are present.
	 */
	public double getMaximumY() {
		List<Node> leafs = de.grogra.pointcloud.utils.Utils.getLeafNodes(this);
		if(leafs.size() == 0)
			return 0;
		double maxY = ((Null)leafs.get(0)).getTranslation().y;
		for(int i = 0; i < leafs.size(); i++) {
			if(maxY < ((Null)leafs.get(i)).getTranslation().y) {
				maxY = ((Null)leafs.get(i)).getTranslation().y;
			}
		}
		return maxY;
	}
	
	/***
	 * Calculates and returns the maximum Z-coordinate value among all leaf nodes.
	 * @return The maximum Z-coordinate value among all leaf nodes, or 0 if no leaf nodes are present.
	 */
	public double getMaximumZ() {
		List<Node> leafs = de.grogra.pointcloud.utils.Utils.getLeafNodes(this);
		if(leafs.size() == 0)
			return 0;
		double maxZ = ((Null)leafs.get(0)).getTranslation().z;
		for(int i = 0; i < leafs.size(); i++) {
			if(maxZ < ((Null)leafs.get(i)).getTranslation().z) {
				maxZ = ((Null)leafs.get(i)).getTranslation().z;
			}
		}
		return maxZ;
	}
	
	@Override
	public PointCloudBase[] split(Attribute a) {
		// TODO Auto-generated method stub
		return null;
	}
	

	public long getSize() {
		return numberOfChild;
	}
	
	private IntermediateCloudNode getLastIPCN() {
		return lastIPCN;
	}
	
	/**
	 * Here we consider the max degree of the graph as balancing factor
	 */
	public long getBalancindFactor() {
		return balancingFactor;
	}
	
	public long getDepth() {
		return depth;
	}
		
	//TODO: probably remove
	public void setMinId(long id) {
		minId=id;
	}
	public void setMaxId(long id) {
		maxId=id;
	}
	
	/*
	 * Return the first leaf imported
	 */
	public PointCloudLeaf getFirstLeaf() {
		Edge e = getFirstEdge();
		while( !(e instanceof PointCloudLeaf) && e!=null) {
			e = e.getNext(e.getTarget());
			if ( !(e instanceof PointCloudBase) && 
					!(e instanceof IntermediateCloudNode) && 
					!(e instanceof PointCloudLeaf)) {
				return null;
			}
		}
		return (PointCloudLeaf) e;
	}
	
	private static void initType ()
	{
		$TYPE.addIdentityAccessor (Attributes.SHAPE);
	}
	
	//enh:insert initType ();
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;

	public static final NType.Field sourceName$FIELD;
	public static final NType.Field childrenClass$FIELD;

	private static final class _Field extends NType.Field
	{
		private final int id;

		_Field (String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			super (PointCloudBaseImpl.$TYPE, name, modifiers, type, componentType);
			this.id = id;
		}

		@Override
		protected void setObjectImpl (Object o, Object value)
		{
			switch (id)
			{
				case 0:
					((PointCloudBaseImpl) o).sourceName = (String) value;
					return;
				case 1:
					((PointCloudBaseImpl) o).childrenClass = (String) value;
					return;
			}
			super.setObjectImpl (o, value);
		}

		@Override
		public Object getObject (Object o)
		{
			switch (id)
			{
				case 0:
					return ((PointCloudBaseImpl) o).getSourceName ();
				case 1:
					return ((PointCloudBaseImpl) o).getChildrenClass ();
			}
			return super.getObject (o);
		}
	}

	static
	{
		$TYPE = new NType (new PointCloudBaseImpl ());
		$TYPE.addManagedField (sourceName$FIELD = new _Field ("sourceName", _Field.PROTECTED  | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (String.class), null, 0));
		$TYPE.addManagedField (childrenClass$FIELD = new _Field ("childrenClass", _Field.PROTECTED  | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (String.class), null, 1));
		initType ();
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new PointCloudBaseImpl ();
	}

	public String getSourceName ()
	{
		return sourceName;
	}

	public String getChildrenClass ()
	{
		return childrenClass;
	}

//enh:end
}
